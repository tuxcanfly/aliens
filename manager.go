package main

import (
	"bytes"
	"fmt"
	"strings"
)

// parse parses the given line into name and directions
//
// NOTE: the expected format is:
// Foo north=Bar west=Baz south=Qu-ux
func parse(line string) (string, map[string]string) {
	fields := strings.Fields(line)
	name := fields[0]
	directions := make(map[string]string)
	for j := 1; j < len(fields); j++ {
		values := strings.Split(fields[j], "=")
		directions[values[0]] = values[1]
	}
	return name, directions
}

// manager represents a map of cities
type manager struct {
	cities map[string]*city
	graph  []*city
}

// newManager constructs a graph with the given input
// input is of the format as a string slice:
//
// Foo north=Bar west=Baz south=Qu-ux
// Bar south=Foo west=Bee
//
// Since there is only one city in any direction
// we will assume a simple graph, not a multigraph
func newManager(in []string) *manager {
	m := &manager{
		cities: make(map[string]*city),
	}
	for i := 0; i < len(in); i++ {
		name, directions := parse(in[i])
		c := m.get(name)
		for direction, name := range directions {
			switch direction {
			case "north":
				c.north = m.get(name)
			case "south":
				c.south = m.get(name)
			case "east":
				c.east = m.get(name)
			case "west":
				c.west = m.get(name)
			}
		}
	}

	// Traverse and complete the graph
	for _, c := range m.cities {
		if c.north != nil {
			c.north.south = c
		}
		if c.south != nil {
			c.south.north = c
		}
		if c.east != nil {
			c.east.west = c
		}
		if c.west != nil {
			c.west.east = c
		}
	}
	return m
}

// get returns a city by name
//
// NOTE: a city name is used as a unique identifier and a new city is created
// if one does not exist by the given name
func (m *manager) get(name string) *city {
	c, ok := m.cities[name]
	if ok {
		return c
	}
	c = newCity(name)
	m.cities[name] = c
	m.graph = append(m.graph, c)
	return c
}

// String returns the map as a serialized string representing the city graph
func (m *manager) String() string {
	var b bytes.Buffer
	for _, c := range m.cities {
		select {
		case <-c.quit:
		default:
			b.WriteString("\n")
			b.WriteString(c.name)

			north := c.North()
			if north != nil {
				b.WriteString(fmt.Sprintf(" north=%v", north))
			}

			south := c.South()
			if south != nil {
				b.WriteString(fmt.Sprintf(" south=%v", south))
			}

			east := c.East()
			if east != nil {
				b.WriteString(fmt.Sprintf(" east=%v", east))
			}

			west := c.West()
			if west != nil {
				b.WriteString(fmt.Sprintf(" west=%v", west))
			}
		}
	}
	return b.String()
}
