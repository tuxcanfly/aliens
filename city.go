package main

// city represents a city
//
// NOTE: neighbor should be considered only if the city is not destroyed, i.e.
// if city.quit is not closed
type city struct {
	name   string
	north  *city
	south  *city
	east   *city
	west   *city
	aliens chan *alien
	quit   chan struct{}
}

// North returns the north neighbor of the city
func (c *city) North() *city {
	if c.north != nil {
		select {
		case <-c.north.quit:
			break
		default:
			return c.north
		}
	}
	return nil
}

// South returns the south neighbor of the city
func (c *city) South() *city {
	if c.south != nil {
		select {
		case <-c.south.quit:
			break
		default:
			return c.south
		}
	}
	return nil
}

// East returns the east neighbor of the city
func (c *city) East() *city {
	if c.east != nil {
		select {
		case <-c.east.quit:
			break
		default:
			return c.east
		}
	}
	return nil
}

// West returns the west neighbor of the city
func (c *city) West() *city {
	if c.west != nil {
		select {
		case <-c.west.quit:
			break
		default:
			return c.west
		}
	}
	return nil
}

// String returns city name
func (c *city) String() string {
	return c.name
}

// newCity creates a new city from the name
func newCity(name string) *city {
	return &city{
		name:   name,
		aliens: make(chan *alien),
		quit:   make(chan struct{}),
	}
}
