package main

import "testing"

// Test that the alien moves as expected
func TestAlien_Move(t *testing.T) {
	var tests = []struct {
		in    *alien
		moves []int
		c     string
	}{
		{
			in: &alien{
				id:   1,
				c:    &city{name: "Foo", north: &city{name: "Bar"}},
				quit: make(chan struct{}),
			},
			moves: []int{0},
			c:     "Bar",
		},
	}

	for i, tt := range tests {
		for i := 0; i < len(tt.moves); i++ {
			tt.in.move(func() int { return tt.moves[i] })
		}
		if tt.in.moves != len(tt.moves) {
			t.Errorf("%d. %q\n\nmoves mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
				i, tt.in, tt.in.moves, len(tt.moves))
		}
		if tt.c != tt.in.c.name {
			t.Errorf("%d. %q\n\ncity mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
				i, tt.in, tt.c, tt.in.c.name)
		}
	}
}
