package main

import "testing"

// Test that the city neighbors are as expected
func TestCity_Neighbors(t *testing.T) {
	var tests = []struct {
		in        *city
		neighbors map[int]string
	}{
		{
			in: &city{
				name:  "India",
				north: &city{name: "Russia"},
				south: &city{name: "Australia"},
				east:  &city{name: "China"},
				west:  &city{name: "US"},
			},
			neighbors: map[int]string{0: "Russia", 1: "Australia", 2: "China", 3: "US"},
		},
	}

	for i, tt := range tests {
		for dir, name := range tt.neighbors {
			switch dir {
			case 0:
				got := tt.in.North()
				if got == nil || got.name != name {
					t.Errorf("%d. %q\n\nnorth mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
						i, tt.in, name, got)
				}
				break
			case 1:
				got := tt.in.South()
				if got == nil || got.name != name {
					t.Errorf("%d. %q\n\nsouth mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
						i, tt.in, name, got)
				}
				break
			case 2:
				got := tt.in.East()
				if got == nil || got.name != name {
					t.Errorf("%d. %q\n\neast mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
						i, tt.in, name, got)
				}
				break
			case 3:
				got := tt.in.West()
				if got == nil || got.name != name {
					t.Errorf("%d. %q\n\nwest mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
						i, tt.in, name, got)
				}
				break
			}
		}
	}
}
