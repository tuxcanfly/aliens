package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// MAXMOVES is the maximum number of moves an alien can make
const MAXMOVES = 10000

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	//  Usage: aliens <input/file/path> N
	var path string
	if len(os.Args) > 1 {
		path = os.Args[1]
	}
	if path == "" || path == "-h" || path == "--help" {
		log.Printf("Usage: aliens <input/file/path> N")
		return
	}

	var N int
	if len(os.Args) > 2 {
		var err error
		N, err = strconv.Atoi(os.Args[2])
		if err != nil {
			log.Fatalf("error reading N: %v", err)
		}
	}

	// Parse and clean the input file
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("error reading file: %v", err)
	}
	var input []string
	lines := strings.Split(string(content), "\n")
	// Skip blank lines
	for _, line := range lines {
		if len(line) > 0 {
			input = append(input, line)
		}
	}

	// Start the manager
	cityManager := newManager(input)

	// Start N aliens and attack, wait for all of them to finish
	var wg sync.WaitGroup
	for i := 0; i < N; i++ {
		// Get a random city to assign to an alien
		c := cityManager.graph[rand.Intn(len(cityManager.graph))]

		// Start the alien attack!
		a := newAlien(i, c)
		wg.Add(1)
		go a.attack(&wg)
	}
	wg.Wait()

	fmt.Printf("%v\n", cityManager)
}
