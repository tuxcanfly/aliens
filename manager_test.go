package main

import "testing"

// Test that the manager parses input into graph
func TestManager_ParseInput(t *testing.T) {
	var tests = []struct {
		in    []string
		graph map[string]map[int]string
	}{
		{
			in:    []string{"Foo"},
			graph: map[string]map[int]string{"Foo": {}},
		},
		{
			in:    []string{"Foo east=Bar"},
			graph: map[string]map[int]string{"Foo": {2: "Bar"}},
		},
	}

	for i, tt := range tests {
		j := 0
		manager := newManager(tt.in)
		for name, directions := range tt.graph {
			city := manager.get(name)
			if name != city.name {
				t.Errorf("%d.%d %q\n\ngraph mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
					i, j, tt.in, name, city.name)
			}
			for direction, name := range directions {
				switch direction {
				case 0:
					got := city.North()
					if got == nil || got.name != name {
						t.Errorf("%d. %q\n\nnorth neighbor mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
							i, tt.in, name, got)
					}
				case 1:
					got := city.South()
					if got == nil || got.name != name {
						t.Errorf("%d. %q\n\nsouth neighbor mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
							i, tt.in, name, got)
					}
				case 2:
					got := city.East()
					if got == nil || got.name != name {
						t.Errorf("%d. %q\n\neast neighbor mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
							i, tt.in, name, got)
					}
				case 3:
					got := city.West()
					if got == nil || got.name != name {
						t.Errorf("%d. %q\n\nwest neighbor mismatch:\n\nexp=%#v\n\ngot=%#v\n\n",
							i, tt.in, name, got)
					}
				}
			}
			j++
		}
	}
}
