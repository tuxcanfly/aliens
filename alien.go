package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// alien represents an alien agent
type alien struct {
	id    int
	moves int
	c     *city
	quit  chan struct{}
}

// direction is a function which returns an int
type direction func() int

// random returns a random int between 0-3
// used to determine direction
func random() int {
	return rand.Intn(4)
}

// move moves an alien to a random direction and handles the situation where an
// alien already exists by sending a message on the city's aliens channel.
//
// NOTE: The moves counter is incremented regardless of whether a city exists
// in the chosen direction. This avoids a potential infinite loop when a city
// has no neighbors.
func (a *alien) move(d direction) {
	switch d() {
	case 0:
		north := a.c.North()
		if north != nil {
			a.c = north
		}
	case 1:
		south := a.c.South()
		if south != nil {
			a.c = south
		}
	case 2:
		east := a.c.East()
		if east != nil {
			a.c = east
		}
	case 3:
		west := a.c.West()
		if west != nil {
			a.c = west
		}
	}
	a.moves++

	select {
	case a.c.aliens <- a:
		select {
		case <-a.c.quit:
		default:
			close(a.c.quit)
			close(a.quit)
		}
	default:
	}
}

// attack runs a loop to move the alien until either a) it has exhausted
// MAXMOVES or b) it receives a message on the city's aliens channel,
// indicating that another alien is attacking, in which case the city is
// destroyed
//
// must be run as a goroutine
func (a *alien) attack(wg *sync.WaitGroup) {
out:
	for {
		a.move(random)
		if a.moves >= MAXMOVES {
			break
		}

		// Assume attack takes a millisecond
		select {
		case b := <-a.c.aliens:
			fmt.Printf("%s has been destroyed by alien %d and alien %d!\n", a.c, a.id, b.id)
			break out
		case <-time.NewTicker(time.Millisecond).C:
		case <-a.quit:
			break out
		}
	}
	wg.Done()
}

// newAlien creates a new alien from the id and city
func newAlien(i int, c *city) *alien {
	return &alien{
		id:    i,
		moves: 0,
		c:     c,
		quit:  make(chan struct{}),
	}
}
