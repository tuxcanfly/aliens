aliens:
=======

👽

`aliens` runs a simulation of an alien attack.

Usage:
------

`aliens <input/file/path> N`

where each line of the input file is of the format:

  Foo north=Bar west=Baz south=Qu-ux

`aliens` creates a graph of the cities in the input, randomly assigns `N`
aliens to cities. Aliens make random moves and destroy a city if two of them
are attacking the same city.

The output is a (pruned) graph of the remaining cities in the same format as
the input.
